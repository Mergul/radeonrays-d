﻿module radeonrays.radeonrays;

import core.stdc.stdint;

import radeonrays.math.float3;
import radeonrays.math.matrix;
import radeonrays.math.quaternion;

enum RADEONRAYS_API_VERSION = 2.0;

extern(C++,"RadeonRays")
{
	//struct Intersection;
	
	/// Represents a device, which can be used by API for intersection purposes.
	/// API is distributing the work across multiple devices itsels, so
	/// this structure is only used to query devices configuration and
	/// limit the number of devices available for the API.
	struct DeviceInfo
	{
		// Device type
		enum Type
		{
			kCpu,
			kGpu,
			kAccelerator
		}
		
		enum Platform
		{
			kOpenCL = 0x1,
			kVulkan = 0x2,
			kEmbree = 0x4,
			
			kAny = 0xFF
		}
		
		// Device name
		const char* name;
		// Device vendor
		const char* vendor;
		// Device type
		Type type;
		Platform platform;
	}
	
	// Forward declaration of entities
	//typedef int Id;
	alias int Id;
	const __gshared Id kNullId = -1;
	
	// Shape interface to repesent intersectable entities
	// The shape is assigned a particular ID which
	// is put by an intersection engine into Intersection structure
	abstract class Shape
	{
	public:
		~this(){}
	
		// World space transform
		void SetTransform(const ref matrix m, const ref matrix minv);
		void GetTransform(ref matrix m, ref matrix minv);

		// Motion blur
		void SetLinearVelocity(const ref float3 v);
		float3 GetLinearVelocity();

		void SetAngularVelocity(const ref quaternion q);
		quaternion GetAngularVelocity();
		
		// ID of a shape
		void SetId(Id id);
		Id GetId();
		
		// Geometry mask to mask out intersections
		void SetMask(int mask);
		int  GetMask();
	}

	// Buffer represents a chunk of memory hosted inside the API
	// (or extrenally in case of interop)
	abstract class Buffer
	{
	public:
		~this(){}
	}
	
	// Synchronization object returned by some API routines.
	abstract class Event
	{
	public:
		~this(){}
		// Indicates whether the related action has been completed
		bool Complete() const;
		// Blocks execution until the event is completed
		void Wait();
	}
	
	// Exception class
	abstract class RRException
	{
	public:
		~this(){}
		const char* what();
	}
	
	// must match Intersection struct on the GPU side exactly!
	struct Intersection
	{
		// Shape ID
		Id shapeid = kNullId;
		// Primitve ID
		Id primid = kNullId;
		
		int padding0;
		int padding1;
		
		// UV parametrization
		float4 uvwt;
	}
	
	enum MapType
	{
		kMapRead = 0x1,
		kMapWrite = 0x2
	}

	// IntersectionApi is designed to provide fast means for ray-scene intersection
	// for AMD architectures. It effectively absracts underlying AMD hardware and
	// software stack and allows user to issue low-latency batched ray queries.
	// The API has 2 major workflows:
	//    - Fast path: the data is expected to be in host memory as well as the returned result is put there
	//    - Complete path: the data can be put into remote memory allocated with API and can be accessed.
	//      by the app directly in remote memory space.
	//
	abstract class IntersectionApi
	{
	public:

		/******************************************
        Backend management
        *******************************************/
		// By default RadeonRays will any platform with potential GPU accelerations,
		// if you prefer to specify which platform call SetPlatform before 
		// GetDeviceInfo/GetDeviceCount for each specific platform
		// By default will choose OpenCL if available, and if not Vulkan
		// Note: this may be sub optimal in some case. to avoid enum all devices
		// across all platforms explicitly before deciding on platform and 
		// device(s) to use
		final static void SetPlatform(const DeviceInfo.Platform platform);
		

		/******************************************
        Device management
        ******************************************/
		// Use this part of API to query for available devices and
		// limit the set of devices which API is going to use
		// Get the number of devices available in the system
		final static uint32_t GetDeviceCount();
		// Get the information for the specified device
		final static void GetDeviceInfo(uint32_t devidx, ref DeviceInfo devinfo);
		
		/******************************************
        API lifetime management
        ******************************************/
		final static IntersectionApi Create(uint32_t devidx);
		
		// Deallocation
		final static void Delete(IntersectionApi api);

		/******************************************
        Geometry manipulation
        ******************************************/
		// Fast path functions to create entities from host memory
		
		// The mesh might be mixed quad\triangle mesh which is determined
		// by numfacevertices array containing numfaces entries describing
		// the number of vertices for current face (3 or 4)
		// The call is blocking, so the returned value is ready upon return.
		Shape CreateMesh(
			// Position data
			const float * vertices, int vnum, int vstride,
			// Index data for vertices
			const int * indices, 
			int istride,
			// Numbers of vertices per face
			const int * numfacevertices,
			// Number of faces
			int  numfaces
			);
		
		// Create an instance of a shape with its own transform (set via Shape interface).
		// The call is blocking, so the returned value is ready upon return.
		Shape CreateInstance(const Shape shape) const;
		// Delete the shape (to simplify DLL boundary crossing
		void DeleteShape(const Shape shape);
		// Attach shape to participate in intersection process
		void AttachShape(const Shape shape);
		// Detach shape, i.e. it is not going to be considered part of the scene anymore
		void DetachShape(const Shape shape);
		// Detach all objects
		void DetachAll();
		// Commit all geometry creations/changes
		void Commit();
		//Sets the shape id allocator to its default value (1)
		void ResetIdCounter();
		//Returns true if no shapes are in the world
		bool IsWorldEmpty();
		
		/******************************************
        Memory management
        ******************************************/
		// Create a buffer to use the most efficient acceleration possible
		Buffer CreateBuffer(size_t size, void* initdata) const;
		// Delete the buffer
		void DeleteBuffer(Buffer buffer) const;
		// Map buffer. Event pointer might be nullptr.
		// The call is asynchronous.
		void MapBuffer(Buffer buffer, MapType type, size_t offset, size_t size, void** data, Event* event) const;
		// Unmap buffer
		void UnmapBuffer(Buffer buffer, void* ptr, Event* event) const;
		
		/******************************************
          Events handling
        *******************************************/
		void DeleteEvent(Event event) const;
		
		/******************************************
          Ray casting
        ******************************************/
		// Complete path:
		// Find closest intersection
		// The call is asynchronous. Event pointers might be nullptrs.
		void QueryIntersection(const Buffer rays, int numrays, Buffer hitinfos, const Event waitevent, Event* event) const;
		// Find any intersection.
		// The call is asynchronous. Event pointer mights be nullptrs.
		void QueryOcclusion(const Buffer rays, int numrays, Buffer hitresults, const Event waitevent, Event* event) const;
		
		// Find closest intersection, number of rays is in remote memory
		// The call is asynchronous. Event pointers might be nullptrs.
		void QueryIntersection(const Buffer rays, const Buffer numrays, int maxrays, Buffer hitinfos, const Event waitevent, Event* event) const;
		// Find any intersection.
		// The call is asynchronous. Event pointer mights be nullptrs.
		void QueryOcclusion(const Buffer rays, const Buffer numrays, int maxrays, Buffer hitresults, const Event waitevent, Event* event) const;
		
		/******************************************
        Utility
        ******************************************/
		// Supported options:
		// option "bvh.type" values {"bvh" (regular bvh, default), "qbvh" (4 branching factor), "hlbvh" (fast builds)}
		// option "bvh.force2level" values {0(default), 1}
		//         by default 2-level BVH is used only if there is instancing in the scene or
		//         motion blur is enabled. 1 forces 2-level BVH for all cases.
		// option "bvh.builder" values {"sah" (use surface area heuristic), "median" (use spatial median, faster to build, default)}
		// option "bvh.sah.use_splits" values {0(default),1} (allow spatial splits for BVH)
		// option "bvh.sah.traversal_cost" values {float, default = 10.f for GPU } (cost of node traversal vs triangle intersection)
		// option "bvh.sah.min_overlap" values { float < 1.f, default = 0.005f } 
		//         (overlap area which is considered for a spatial splits, fraction of parent bbox)
		// option "bvh.sah.max_split_depth" values {int, default = 10} (max depth in the tree where spatial split can happen)
		// option "bvh.sah.extra_node_budget" values {float, default = 1.f} (maximum node memory budget compared to normal bvh (2*num_tris - 1), for ex. 0.3 = 30% more nodes allowed
		// Set API global option: string
		void SetOption(const char* name, const char* value);
		// Set API global option: float
		void SetOption(const char* name, float value);

	protected:
		
		~this(){}
	}
}
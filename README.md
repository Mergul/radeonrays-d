radeonrays-d
==========

A static bindings to RadeonRays for the D Programming Language. Bindings 
require some modification in RadeonRays code to work. At this time, bindings contain
only a part of funcionality of library.

Library github page: "https://github.com/GPUOpen-LibrariesAndSDKs/RadeonRays_SDK"